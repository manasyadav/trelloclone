import {
  Card,
  CardContent,
  FormControl,
  TextField,
  Box,
  Button,
} from "@mui/material";

import CloseIcon from "@mui/icons-material/Close";



export default function AddExtends({type, itemName, setItemName, addItem, handleClose}) {

  const handleSubmit = (e) => {
    e.preventDefault(); 
    addItem(); 
  };

  return (
    <Card
      sx={{
        borderRadius: 5,
        textAlign: "left",
        height: 150,
        width: 300,
        backgroundColor: `#0e1404`,
      }}
      style={type === 'Item' ? { backgroundColor:"#364047", boxShadow:"none"} : {}}
    >
      <CardContent>
        <form onSubmit={handleSubmit}>
        <FormControl sx={{ display: "flex", gap: 3 }}>
          <TextField
            type="text"
            placeholder={`Enter ${type} title...`}
            sx={{ "& input": { color: "white" } }}
            onChange={(e) => {
              setItemName(e.target.value);
            }}
            value={itemName}
          />
          <Box sx={{ display: "flex", gap: 2, alignItems: "center" }}>
            <Button
              variant="contained"
              style={{ color: "black" }}
              type="submit"
            >
              Add {type}
            </Button>
            <CloseIcon style={{ color: "white" }} onClick={handleClose} />
          </Box>
        </FormControl>
        </form>
      </CardContent>
    </Card>
  );
}
