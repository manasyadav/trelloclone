import { Box, ButtonBase } from "@mui/material";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";

import axios from "axios";

import { useState, useEffect } from "react";

import fetchData from "../fetchData";
import Board from "./Board";

import ReactLoading from "react-loading";
import AddPopover from "./AddPopover";
import ErrorComponent from "./ErrorComponent";

const TOKEN = import.meta.env.VITE_TOKEN;
const KEY = import.meta.env.VITE_API_KEY;

export default function BoardContainer() {
  const [anchorEl, setAnchorEl] = useState(null);
  const [createBoard, setCreateBoard] = useState(false);
  const [boardName, setBoardName] = useState("");
  const [boardList, setBoardList] = useState([]);
  const [loader, setLoader] = useState(false);
  const [error, setError] = useState("");

  

  useEffect(() => {
    fetchData(setBoardList, "members", "boards", setError, setLoader);
  }, []);

  function handleClick(e) {
    setAnchorEl(e.currentTarget);
    setCreateBoard(true);
  }

  function handleClose() {
    setCreateBoard(false);
  }

  async function addBoard() {
    try {
      let res = await axios.post(
        `https://api.trello.com/1/boards/?name=${boardName}&key=${KEY}&token=${TOKEN}`
      );
      handleClose();
      setBoardList((boardList) => [...boardList, res.data]);
    } catch (error) {
      handleClose();
      setError(`Couldn't add the board`);
    }
  }

  return (
    <Box
      sx={{
        display: "flex",
        justifyContent: "center",
      }}
    >
      {error && <ErrorComponent errorType={error} />}
      {loader ? (
        <Box
          sx={{
            display: "flex",
            flexWrap: "wrap",
            justifyContent: "flex-start",
            gap: 2,
            padding: 5,
          }}
        >
          <ButtonBase onClick={handleClick}>
            <Card
              sx={{
                backgroundColor: "#262d33",
                width: 300,
                height: 150,
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <CardContent>
                <Typography fontSize={20} fontWeight={500} color="#86929d">
                  Create new board
                </Typography>
              </CardContent>
            </Card>
          </ButtonBase>
          <AddPopover
            type={"Board"}
            anchorEl={anchorEl}
            createItem={createBoard}
            handleClose={handleClose}
            addItem={addBoard}
            setItemName={setBoardName}
          />

          {boardList.map((board, index) => {
            return <Board board={board} index={index} />;
          })}
        </Box>
      ) : (
        <ReactLoading
          type={"spin"}
          color={"grey"}
          height={"10%"}
          width={"10%"}
        />
      )}
    </Box>
  );
}
