import {
  Box,
  Button,
  Checkbox,
  FormControlLabel,
  FormGroup,
  LinearProgress,
  Typography,
} from "@mui/material";
import { useEffect, useState } from "react";
import fetchData from "../fetchData";
import Delete from "@mui/icons-material/Delete";
import axios from "axios";

import ErrorComponent from "./ErrorComponent.jsx";

import ReactLoading from "react-loading";
import AddExtends from "./AddExtends.jsx";
import { deleteItem } from "../deleteApi.js";

const TOKEN = import.meta.env.VITE_TOKEN;
const KEY = import.meta.env.VITE_API_KEY;

const buttonStyle = {
  backgroundColor: "#46545e",
  boxShadow: "none",
  color: "#9cacbc",
  "&:hover": { backgroundColor: "#556773" },
};

export default function CheckItemContainer({ id, cardId }) {
  const [checkItems, setCheckItems] = useState([]);
  const [loaded, setLoaded] = useState(false);
  const [error, setError] = useState("");
  const [createItem, setCreateItem] = useState(true);
  const [itemName, setItemName] = useState("");

  useEffect(() => {
    fetchData(
      setCheckItems,
      "checklists",
      "checkItems",
      setError,
      setLoaded,
      id
    );
  }, []);

  function handleClose() {
    setCreateItem(true);
  }

  async function addItem() {
    try {
      let res = await axios.post(
        `https://api.trello.com/1/checklists/${id}/checkItems?name=${itemName}&key=${KEY}&token=${TOKEN}`
      );
      setCheckItems((checkItems) => [...checkItems, res.data]);
      setItemName("");
    } catch (error) {
      setError(`Couldn't add Item`);
    }
  }

  function removeItem(itemId) {
    try {
      deleteItem(id,itemId);
      setCheckItems((checkItems) =>
        checkItems.filter((checkItem) => checkItem.id !== itemId)
      );
    } catch (error) {
      setError(`Couldn't delete CheckItem`);
    }
  }

  async function handleCheck(itemId, state) {
    try {
      let value = state === "complete" ? "incomplete" : "complete";
      await axios.put(
        `https://api.trello.com/1/cards/${cardId}/checkItem/${itemId}?state=${value}&key=${KEY}&token=${TOKEN}`
      );
      setCheckItems((checkItems) =>
        checkItems.map((item) => {
          if (item.id === itemId) {
            item.state = value;
          }
          return item;
        })
      );
    } catch (error) {
      setError(`Couldn't check item`);
    }
  }

  function progressTracker() {
    let checked = 0;
    checkItems.forEach((item) => {
      if (item.state === "complete") {
        checked++;
      }
    });
    let progressCalc = (checked / checkItems.length) * 100 || 0;
    console.log(progressCalc);
    return parseInt(progressCalc);
  }

  return (
    <>
      {error && <ErrorComponent errorType={error} />}
      <Box sx={{ padding: 1 }}>
        {loaded ? (
          <>
            <Box sx={{ display: "flex", alignItems: "center", gap: 1 }}>
              <Typography sx={{ fontSize: 15 }}>
                {progressTracker()}%
              </Typography>
              <LinearProgress
                variant="determinate"
                color={progressTracker() === 100 ? "success" : "primary"}
                value={progressTracker()}
                sx={{ width: "95%" }}
              />
            </Box>
            <FormGroup>
              {checkItems.map((checkitem) => {
                return (
                  <Box
                    sx={{
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "space-between",
                    }}
                  >
                    <FormControlLabel
                      key={checkitem.id}
                      control={
                        <Checkbox
                          onClick={() =>
                            handleCheck(checkitem.id, checkitem.state)
                          }
                          checked={
                            checkitem.state === "complete" ? true : false
                          }
                        />
                      }
                      label={checkitem.name}
                    />
                    <Delete onClick={() => removeItem(checkitem.id)} />
                  </Box>
                );
              })}
            </FormGroup>
            {createItem ? (
              <Button sx={buttonStyle} onClick={() => setCreateItem(false)}>
                Add an item
              </Button>
            ) : (
              <AddExtends
                type={"Item"}
                itemName={itemName}
                setItemName={setItemName}
                addItem={addItem}
                handleClose={handleClose}
              />
            )}
          </>
        ) : (
          <ReactLoading
            type={"spin"}
            color={"grey"}
            height={"10%"}
            width={"10%"}
          />
        )}
      </Box>
    </>
  );
}
