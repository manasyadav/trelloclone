import { Box, ButtonBase, Card, CardContent, Typography } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";

export default function AddCard({handleClick}) {
  return (
    <Box sx={{ display: "flex", justifyContent: "center" }}>
      <ButtonBase onClick={handleClick}>
        <Card
          sx={{
            borderRadius: 2,
            width: 250,
            backgroundColor: `#0e1404`,
            "&:hover": { backgroundColor: "#26352b" },
          }}
        >
          <CardContent
            style={{ paddingBottom: 0, padding: 10 }}
            sx={{ display: "flex", justifyContent: "space-between" }}
          >
            <AddIcon style={{ color: "#94a0ac" }} />
            <Typography fontSize={17} color="#94a0ac">
              Add a card
            </Typography>
          </CardContent>
        </Card>
      </ButtonBase>
    </Box>
  );
}
