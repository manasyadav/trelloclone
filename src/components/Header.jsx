import * as React from "react";

import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";

import logo from "../assets/TrelloLogo.svg";



export default function Header() {
  return (
    <Box sx={{flexGrow:1}}>
      <AppBar position="static" sx={{ backgroundColor: "#1c2125", }}>
        <Toolbar sx={{ gap: 3 }}>
          <img src={logo} height="25rem" />
        </Toolbar>
      </AppBar>
    </Box>
  );
}
