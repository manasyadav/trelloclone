import { Alert, Snackbar } from "@mui/material";
import { useState } from "react";

export default function ErrorComponent({ errorType }) {
  const [open, setOpen] = useState(true);

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
  };

  return (
    <Snackbar 
      open={open} 
      autoHideDuration={6000} 
      onClose={handleClose}
      anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }} 
    >
      <Alert
        onClose={handleClose}
        severity="error"
        variant="filled"
        sx={{ width: "400px", fontSize: "18px" }} 
      >
        {errorType}
      </Alert>
    </Snackbar>
  );
}
