import Delete from "@mui/icons-material/Delete";
import {
  Box,
  ButtonBase,
  Card,
  CardContent,
  Modal,
  Typography,
} from "@mui/material";

import CheckListContainer from "./CheckListContainer";
import { useState } from "react";

export default function Cards({ card, index, deleteCard }) {
  const [open, setOpen] = useState(false);

  function handleOpen() {
    setOpen(true);
  }

  function handleClose() {
    setOpen(false);
  }

  return (
    <Box>
      <Box sx={{ display: "flex", justifyContent: "center" }}>
        <Card
          key={index}
          sx={{
            paddingX: 1,
            borderRadius: 2,
            width: 250,
            backgroundColor: `#21272b`,
          }}
        >
          <CardContent
            style={{ paddingBottom: 0, padding: 10 }}
            sx={{ display: "flex", justifyContent: "space-between" }}
          >
            <Box onClick={handleOpen} flex={2}>
              <Typography fontSize={17} color="#94a0ac">
                {card.name}
              </Typography>
            </Box>
            <ButtonBase flex={1} onClick={() => deleteCard(card.id)}>
              <Delete style={{ color: "#94a0ac" }} />
            </ButtonBase>
          </CardContent>
        </Card>
      </Box>
      <Modal open={open} onClose={handleClose} sx={{ overflow: "scroll" }}>
        <CheckListContainer name={card.name} id={card.id} />
      </Modal>
    </Box>
  );
}
