import { ButtonBase, Card, CardContent, Typography } from "@mui/material";
import { Link } from "react-router-dom";

export default function Board({board,index}){
    return (
        <ButtonBase>
            <Link to={`/boards/${board.id}`}>
            <Card
              key={index}
              sx={{
                textAlign:"left",
                width: 300,
                height: 150,
                backgroundColor: `${
                  board.prefs.backgroundColor
                    ? board.prefs.backgroundColor
                    : "grey"
                }`,
              }}
            >
              <CardContent>
                <Typography fontSize={20} fontWeight={700} color="white">
                  {board.name}
                </Typography>
              </CardContent>
            </Card>
            </Link>
          </ButtonBase>
    );
}