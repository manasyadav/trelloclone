import { ButtonBase, Card, CardContent, Typography } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";

export default function AddList({ handleClick }) {
  return (
    <ButtonBase onClick={handleClick} sx={{ alignSelf: "flex-start" }}>
      <Card
        sx={{
          backgroundColor: "#2a75a2",
          borderRadius: 5,
          width: 350,
          height: 70,
          display: "flex",
          alignItems: "center",
          "&:hover": { backgroundColor: "#005c93" }
        }}
      >
        <CardContent>
          <Typography
            fontSize={20}
            fontWeight={500}
            color="white"
            display="flex"
            alignItems="center"
          >
            <AddIcon />
            Add another list
          </Typography>
        </CardContent>
      </Card>
    </ButtonBase>
  );
}
