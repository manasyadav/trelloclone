import React, { useEffect, useState } from "react";
import fetchData from "../fetchData";
import { Box } from "@mui/material";
import axios from "axios";
import Cards from "./Cards";

import ErrorComponent from "./ErrorComponent";

import ReactLoading from "react-loading";

import AddCard from "./AddCard";
import AddExtends from "./AddExtends";
import { deleteCard } from "../deleteApi";

const TOKEN = import.meta.env.VITE_TOKEN;
const KEY = import.meta.env.VITE_API_KEY;

export default function CardContainer({ id }) {
  const [cards, setCards] = useState([]);
  const [createCard, setCreateCard] = useState(false);
  const [cardName, setCardName] = useState("");
  const [loader, setLoader] = useState(false);
  const [error, setError] = useState("");

  useEffect(() => {
    fetchData(setCards, "lists", "cards", setError, setLoader, id);
  }, []);

  function handleClick(e) {
    setCreateCard(true);
  }

  function handleClose() {
    setCreateCard(false);
  }

  async function addCard() {
    try {
      let res = await axios.post(
        `https://api.trello.com/1/cards?name=${cardName}&idList=${id}&key=${KEY}&token=${TOKEN}`
      );
      setCards((cards) => [...cards, res.data]);
      setCardName("");
    } catch (error) {
      setError(`Couldn't add Card`);
    }
  }

  function removeCard(id) {
    try {
      deleteCard(id);
      setCards((oldCards) => oldCards.filter((card) => card.id !== id));
    } catch (error) {
      setError(`Couldn't delete card`);
    }
  }

  return (
    <>
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          gap: 2,
          paddingBottom: 2,
        }}
      >
        {error && <ErrorComponent errorType={error} />}
        {loader ? (
          cards.map((card, index) => {
            return <Cards card={card} index={index} deleteCard={removeCard} />;
          })
        ) : (
          <Box sx={{ display: "flex", justifyContent: "center" }}>
            <ReactLoading
              type={"spin"}
              color={"grey"}
              height={"10%"}
              width={"10%"}
            />
          </Box>
        )}
        {!createCard ? (
          <AddCard handleClick={handleClick} />
        ) : (
          <AddExtends
            type={"Card"}
            itemName={cardName}
            setItemName={setCardName}
            addItem={addCard}
            handleClose={handleClose}
          />
        )}
      </Box>
    </>
  );

}
