import {
  Popover,
  Box,
  Typography,
  FormControl,
  FormLabel,
  TextField,
  Button,
} from "@mui/material";

export default function AddPopover({
  type,
  anchorEl,
  createItem,
  handleClose,
  addItem,
  setItemName,
}) {
  const handleSubmit = (e) => {
    e.preventDefault(); 
    addItem(); 
  };

  return (
    <Popover
      anchorEl={anchorEl}
      open={createItem}
      onClose={handleClose}
      anchorOrigin={{
        vertical: "top",
        horizontal: "right",
      }}
    >
      <Box
        display="flex"
        flexDirection="column"
        rowGap={5}
        sx={{
          padding: 5,
          alignItems: "center",
          backgroundColor: "#272e33",
        }}
      >
        <Typography fontSize={20} fontWeight={700} sx={{ color: "#9cacbc" }}>
          Create {type}
        </Typography>
        <form onSubmit={handleSubmit}>
          <FormControl sx={{ display: "flex", gap: 3 }}>
            <FormLabel sx={{ color: "#9cacbc" }}>{type} Title</FormLabel>
            <TextField
              type="text"
              sx={{
                backgroundColor: "#1d2326",
                "& input": { color: "#9cacbc" },
              }}
              onChange={(e) => {
                setItemName(e.target.value);
              }}
            />
            <Button variant="contained" type="submit" sx={{ color: "#1d2326" }}>
              Create {type}
            </Button>
          </FormControl>
        </form>
      </Box>
    </Popover>
  );
}
