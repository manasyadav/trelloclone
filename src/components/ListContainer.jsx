import React, { useEffect, useState } from "react";
import { useParams, Link } from "react-router-dom";
import axios from "axios";

import fetchData from "../fetchData";
import Lists from "./List";

import { Box, Button, List, ListItem, Typography } from "@mui/material";

import AddList from "./AddList";

import ReactLoading from "react-loading";
import AddExtends from "./AddExtends";
import ErrorComponent from "./ErrorComponent";
import { deleteList } from "../deleteApi";

const TOKEN = import.meta.env.VITE_TOKEN;
const KEY = import.meta.env.VITE_API_KEY;

const buttonStyle = {
  backgroundColor: "#46545e",
  boxShadow: "none",
  color: "#9cacbc",
  "&:hover": { backgroundColor: "#556773" },
  marginLeft: 10,
  marginTop: 5,
};

const containerStyle = {
  display: "flex",
  overflowX: "auto",
  justifyContent: "flex-start",
};

export default function ListContainer() {
  const [lists, setLists] = useState([]);
  const [listName, setListName] = useState("");
  const [createList, setCreateList] = useState(false);
  const [loader, setLoader] = useState(false);
  const [error, setError] = useState("");

  const id = useParams().id;

  useEffect(() => {
    fetchData(setLists, "boards", "lists", setError, setLoader, id);
  }, []);

  function handleClick() {
    setCreateList(true);
  }

  function handleClose() {
    setCreateList(false);
  }

  async function addList() {
    try {
      let res = await axios.post(
        `https://api.trello.com/1/lists?name=${listName}&idBoard=${id}&key=${KEY}&token=${TOKEN}`
      );
      setLists((lists) => [...lists, res.data]);
      setListName("");
    } catch (error) {
      setError(error.message);
    }
  }

  function deleteListItem(id) {
    try {
      deleteList(id);
      setLists((oldList) => oldList.filter((list) => list.id !== id));
    } catch (error) {
      setError(error.message);
    }
  }

  return (
    <Box>
      <Link to={"/boards"}>
        <Button sx={buttonStyle}>Boards</Button>
      </Link>
      {error === "400" ? (
        <Typography
          sx={{
            color: "#fff",
            fontSize: 35,
            display: "flex",
            justifyContent: "center",
          }}
        >
          Board doesn't exist.
        </Typography>
      ) : (
        <>
          {error !== "" && error !== "400" && (
            <ErrorComponent errorType={error} />
          )}
          {loader ? (
            <Box
              sx={{
                marginLeft: 10,
                gap: 2,
                padding: 5,
              }}
              style={containerStyle}
            >
              <List sx={{ display: "flex", alignItems: "flex-start" }}>
                {lists.map((list, index) => {
                  return (
                    <ListItem key={list.id}>
                      <Lists
                        list={list}
                        index={index}
                        deleteList={deleteListItem}
                      />
                    </ListItem>
                  );
                })}
                {!error && (
                  <ListItem>
                    {!createList ? (
                      <AddList handleClick={handleClick} />
                    ) : (
                      <AddExtends
                        type={"List"}
                        itemName={listName}
                        setItemName={setListName}
                        addItem={addList}
                        handleClose={handleClose}
                      />
                    )}
                  </ListItem>
                )}
              </List>
            </Box>
          ) : (
            <Box sx={{ display: "flex", justifyContent: "center" }}>
              <ReactLoading
                type={"spin"}
                color={"grey"}
                height={"10%"}
                width={"10%"}
              />
            </Box>
          )}
        </>
      )}
    </Box>
  );
}
