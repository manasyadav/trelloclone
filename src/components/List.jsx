import {
  Button,
  Card,
  CardContent,
  Typography,
} from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import CardContainer from "./CardContainer";

export default function Lists({ list, index, deleteList }) {
  return (
      <Card
        key={index}
        sx={{
          borderRadius: 5,
          width: 300,
          minHeight: 150,
          backgroundColor: `#0e1404`,
        }}
      >
        <CardContent sx={{ display: "flex", justifyContent: "space-between", paddingLeft:4}}>
          <Typography fontSize={20} fontWeight={700} color="#b4c1cf">
            {list.name}
          </Typography>
          <Button
            onClick={() => {
              deleteList(list.id);
            }}
          >
            <DeleteIcon sx={{ color: "#94a0ac" }} />
          </Button>
        </CardContent>
        <CardContainer id={list.id} />
      </Card>
  );
}
