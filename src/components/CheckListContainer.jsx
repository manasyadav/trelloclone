import { useEffect, useState } from "react";
import axios from "axios";

import { Box, Button, Typography } from "@mui/material";
import CreditCardIcon from "@mui/icons-material/CreditCard";
import CheckBoxOutlinedIcon from "@mui/icons-material/CheckBoxOutlined";

import ReactLoading from "react-loading";

import ErrorComponent from "./ErrorComponent.jsx";
import fetchData from "../fetchData.js";
import CheckItemContainer from "./CheckItemContainer.jsx";
import AddPopover from "./AddPopover.jsx";
import { deleteCheckList } from "../deleteApi.js";

const style = {
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  justifyContent: "space-between",
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -25%)",
  width: 400,
  bgcolor: "#364047",
  color: "#9cacbc",
  borderRadius: 3,
  p: 4,
  marginTop: -20,
};

const buttonStyle = {
  backgroundColor: "#46545e",
  boxShadow: "none",
  color: "#9cacbc",
  "&:hover": { backgroundColor: "#556773" },
};

const TOKEN = import.meta.env.VITE_TOKEN;
const KEY = import.meta.env.VITE_API_KEY;

export default function CheckListContainer({ name, id }) {
  const [anchorEl, setAnchorEl] = useState(null);
  const [createCheckList, setCreateCheckList] = useState(false);
  const [checklistName, setChecklistName] = useState("");
  const [checklists, setChecklists] = useState([]);
  const [loader, setLoader] = useState(false);
  const [error, setError] = useState("");

  useEffect(() => {
    fetchData(setChecklists, "cards", "checklists", setError, setLoader, id);
  }, []);

  function handleClick(e) {
    setAnchorEl(e.currentTarget);
    setCreateCheckList(true);
  }

  function handleClose() {
    setCreateCheckList(false);
  }

  async function addCheckList() {
    try {
      let res = await axios.post(
        `https://api.trello.com/1/checklists?name=${checklistName}&idCard=${id}&key=${KEY}&token=${TOKEN}`
      );

      setChecklists((checklists) => [...checklists, res.data]);
    } catch (error) {
      setError(`Couldn't add Checklist`);
    }
  }

  function removeCheckList(id) {
    try {
      deleteCheckList(id);
      setChecklists((checklists) =>
        checklists.filter((checklist) => checklist.id !== id)
      );
    } catch (error) {
      setError(`Couldn't delete Checklist`);
    }
  }

  return (
    <>
      {error && <ErrorComponent errorType={error} />}
      <Box sx={style}>
        {loader ? (
          <>
            <Box
              sx={{
                display: "flex",
                width: "100%",
                justifyContent: "space-between",
              }}
            >
              <Box sx={{ display: "flex", alignItems: "center", gap: 1 }}>
                <CreditCardIcon />
                <Typography sx={{ fontSize: 25, fontWeight: 600 }}>
                  {name}
                </Typography>
              </Box>
              <Button onClick={handleClick} sx={buttonStyle}>
                Create Checklist
              </Button>
            </Box>
            <AddPopover
              type={"Checklist"}
              anchorEl={anchorEl}
              createItem={createCheckList}
              handleClose={handleClose}
              addItem={addCheckList}
              setItemName={setChecklistName}
            />
            <Box sx={{ marginTop: 5, width: "100%" }}>
              {checklists.map((checklist) => {
                return (
                  <Box
                    key={checklist.id}
                    sx={{
                      paddingBottom: 4,
                      display: "flex",
                      flexDirection: "column",
                    }}
                  >
                    <Box
                      sx={{
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "space-between",
                      }}
                    >
                      <Box
                        sx={{ display: "flex", alignItems: "center", gap: 2 }}
                      >
                        <CheckBoxOutlinedIcon />
                        <Typography sx={{ fontWeight: 600, fontSize: 23 }}>
                          {checklist.name}
                        </Typography>
                      </Box>
                      <Button
                        sx={buttonStyle}
                        onClick={() => removeCheckList(checklist.id)}
                      >
                        Delete
                      </Button>
                    </Box>
                    <Box>
                      <CheckItemContainer id={checklist.id} cardId={id} />
                    </Box>
                  </Box>
                );
              })}
            </Box>
          </>
        ) : (
          <ReactLoading
            type={"spin"}
            color={"grey"}
            height={"10%"}
            width={"10%"}
          />
        )}
      </Box>
    </>
  );
}
