import { Navigate, createBrowserRouter } from "react-router-dom";
import BoardContainer from "./components/BoardContainer";
import ListContainer from "./components/ListContainer";

function routerFunction() {
  const router = createBrowserRouter([
    {
      path: "/",
      element: <Navigate to="/boards" replace={true}/>,
    },
    {
      path: "/boards",
      element: <BoardContainer />,
    },
    {
      path: "/boards/:id",
      element: <ListContainer/>,
    }
  ]);

  return router;
}

export default routerFunction;
