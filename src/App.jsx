import "./App.css";
import Header from "./components/Header";
import { RouterProvider } from "react-router-dom";
import routerFunction from "./router";
import { Box } from "@mui/material";


function App() {
  return (
    <Box >
      <Header />
      <RouterProvider router={routerFunction()}/>
    </Box>
  );
}

export default App;
