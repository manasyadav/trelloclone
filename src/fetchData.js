import axios from "axios";

const TOKEN = import.meta.env.VITE_TOKEN;
const KEY = import.meta.env.VITE_API_KEY;

async function fetchData(setData, from, item, setError, setLoader, id = "me") {
  try {
    let response = await axios.get(
      `https://api.trello.com/1/${from}/${id}/${item}?key=${KEY}&token=${TOKEN}`
    );
    let data = response.data;
    setLoader(true);
    setData(data);
  } catch (e) {
    setLoader(true);
    console.log(e.response.status);
    if(e.response.status === 400){
      setError(`400`);
    }
    else {
      setError(`Couldn't get ${item}`);
    }
  }
}

export default fetchData;
