import axios from "axios";

const TOKEN = import.meta.env.VITE_TOKEN;
const KEY = import.meta.env.VITE_API_KEY;

axios.defaults.params = {
  key : KEY,
  token : TOKEN
};

export async function deleteList(id) {
  await axios.put(
    `https://api.trello.com/1/lists/${id}?closed=true`
  );
}

export async function deleteCard(id) {
  await axios.delete(
    `https://api.trello.com/1/cards/${id}`
  );
}

export async function deleteCheckList(id) {
  await axios.delete(
    `https://api.trello.com/1/checklists/${id}`
  );
}

export async function deleteItem(id, itemId) {
  await axios.delete(
    `https://api.trello.com/1/checklists/${id}/checkItems/${itemId}`
  );
}
